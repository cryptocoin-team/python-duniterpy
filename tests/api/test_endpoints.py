# Copyright  2014-2022 Vincent Texier <vit@free.fr>
#
# DuniterPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DuniterPy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from duniterpy.api import endpoint


class TestEndpoint(unittest.TestCase):
    def test_gva(self):
        endpoint_str = "GVA test.domain.com 127.0.0.1 2001:0db8:0000:85a3:0000:0000:ac1f:8001 10902 gva"

        gva_endpoint = endpoint.GVAEndpoint.from_inline(endpoint_str)

        self.assertEqual(gva_endpoint.flags, "")
        self.assertEqual(gva_endpoint.host, "test.domain.com")
        self.assertEqual(gva_endpoint.ipv4, "127.0.0.1")
        self.assertEqual(gva_endpoint.ipv6, "2001:0db8:0000:85a3:0000:0000:ac1f:8001")
        self.assertEqual(gva_endpoint.port, 10902)
        self.assertEqual(gva_endpoint.path, "gva")

        self.assertEqual(gva_endpoint.inline(), endpoint_str)

        endpoint_str = "GVA S test.domain.com 10902 gva"

        gva_endpoint = endpoint.GVAEndpoint.from_inline(endpoint_str)

        self.assertEqual(gva_endpoint.flags, "S")
        self.assertEqual(gva_endpoint.host, "test.domain.com")
        self.assertEqual(gva_endpoint.ipv4, None)
        self.assertEqual(gva_endpoint.ipv6, None)
        self.assertEqual(gva_endpoint.port, 10902)
        self.assertEqual(gva_endpoint.path, "gva")

        self.assertEqual(gva_endpoint.inline(), endpoint_str)

        endpoint_str = "GVA S xn--duniter.org 10902"

        gva_endpoint = endpoint.GVAEndpoint.from_inline(endpoint_str)

        self.assertEqual(gva_endpoint.flags, "S")
        self.assertEqual(gva_endpoint.host, "xn--duniter.org")
        self.assertEqual(gva_endpoint.ipv4, None)
        self.assertEqual(gva_endpoint.ipv6, None)
        self.assertEqual(gva_endpoint.port, 10902)
        self.assertEqual(gva_endpoint.path, "")

        self.assertEqual(gva_endpoint.inline(), endpoint_str)

    def test_gva_host_ipv4_mix_up(self):
        endpoint_str = "GVA S 127.0.0.1 443 gva"
        gva_endpoint = endpoint.GVAEndpoint.from_inline(endpoint_str)
        self.assertEqual(gva_endpoint.host, "")
        self.assertEqual(gva_endpoint.ipv4, "127.0.0.1")

    def test_bmas_host_ipv4_mix_up(self):
        endpoint_str = "BMAS 127.0.0.1 443 bma"
        bmas_endpoint = endpoint.SecuredBMAEndpoint.from_inline(endpoint_str)
        self.assertEqual(bmas_endpoint.host, "")
        self.assertEqual(bmas_endpoint.ipv4, "127.0.0.1")
