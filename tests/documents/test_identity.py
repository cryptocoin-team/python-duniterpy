# Copyright  2014-2022 Vincent Texier <vit@free.fr>
#
# DuniterPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DuniterPy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from duniterpy.constants import G1_TEST_CURRENCY_CODENAME
from duniterpy.documents import Identity

PUBKEY = "A"
UID = "test"
BLOCK_ID = "0-NRSATU"
IDENTITY_KWARGS = [PUBKEY, UID, BLOCK_ID]


def test_identity_equality():
    idty1 = Identity(*IDENTITY_KWARGS)
    idty2 = Identity(*IDENTITY_KWARGS)
    assert idty1 == idty2


@pytest.mark.parametrize(
    "pubkey, uid, block_id, currency",
    [
        ("pubkey", UID, BLOCK_ID, None),
        (PUBKEY, "uid", BLOCK_ID, None),
        (PUBKEY, UID, "1-TEST", None),
        IDENTITY_KWARGS + [G1_TEST_CURRENCY_CODENAME],
    ],
)
def test_identity_inequality(pubkey, uid, block_id, currency):
    idty1 = Identity(*IDENTITY_KWARGS)
    idty2 = Identity(pubkey, uid, block_id)
    if currency:
        idty2.currency = currency
    assert not idty1 == idty2
