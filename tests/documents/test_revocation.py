# Copyright  2014-2022 Vincent Texier <vit@free.fr>
#
# DuniterPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DuniterPy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from duniterpy.constants import G1_TEST_CURRENCY_CODENAME
from duniterpy.documents import Identity, Revocation

PUBKEY = "A"
UID = "test"
BLOCK_ID = "0-NRSATU"
IDENTITY_KWARGS = [PUBKEY, UID, BLOCK_ID]


def test_revocation_equality():
    rev1 = Revocation(Identity(*IDENTITY_KWARGS))
    rev2 = Revocation(Identity(*IDENTITY_KWARGS))
    assert rev1 == rev2


@pytest.mark.parametrize(
    "pubkey, uid, block_id, currency",
    [
        ("pubkey", UID, BLOCK_ID, None),
        (PUBKEY, "uid", BLOCK_ID, None),
        (PUBKEY, UID, "1-TEST", None),
        IDENTITY_KWARGS + [G1_TEST_CURRENCY_CODENAME],
    ],
)
def test_revocation_inequality(pubkey, uid, block_id, currency):
    rev1 = Revocation(Identity(*IDENTITY_KWARGS))
    rev2 = Revocation(Identity(pubkey, uid, block_id))
    if currency:
        rev2.currency = currency
    assert not rev1 == rev2
