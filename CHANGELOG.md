## v1.1.1 (14th August 2023)

Bug fix release, because `libnacl` brought in a breaking change.

### Fixes
- #206, !190: Fix `libnacl` version to 1.8.0
- Fix deprecation warning about `typing.re.Pattern`

### Backport necessary changes from v1.2.0 development branch for beeing able to release
- #194: Introduce `tests:3.11` job
  - to set `PYTHON_VERSION` to `3.11` in order to fix the pipeline since `pre-commit` is now installed on `3.11` image
- Remove runners tags
- Bump broken isort, black, pylint pre-commit hooks

## [v1.1.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/18) (14th February 2022)

### Project
- #179, !172: Drop Python v3.6 support
- #188, !168: Introduce Python 3.10 support
- #145, !171: Bump copyright to 2022

### Code
- #187, !167: Implement `__eq__` and `__hash__` `Documents` methods
- #146, !166: Use `f-string`

### Dev Env/CI
- #90, #103, !113: Pytest introduction, coverage report and badge generations
- #165, !170: Stop using the Makefile in the CI and tools installed from Poetry dev dependencies in favor of pre-commit
- #186, !164: Introduce `pyupgrade` pre-commit hook
- !163: pylint: Remove `R0401` and `R0904` disables
- !169: Bump pre-commit hooks version, Handle new Pylint rules
- #182, !170: Bump Sphinx to v4

---

- Thanks @Moul, @vtexier

## [v1.0.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/12) (21st August 2021)
**See below the main changes for the v1.0**

No changes since the rc1.

## [v1.0.0rc1](https://git.duniter.org/clients/python/duniterpy/-/milestones/12) (25th July 2021)
**See below the main changes for the v1.0**

### Fixes
- #152, #170, !159: Fix `ESUserEndpoint` regex
  - Fix datapod domain name in `request_data_elasticsearch` example

### Development Environment
- #149, !161: Change workflow by changing from `master` and default `dev` branches to a single `main` branch
- Add changelog link to PyPI

### Documentation
- !160: Fix autodoc generation by running it into Poetry virtualenv
- !158: Allow to trigger the documentation job without having the release stage completed

## [v1.0.0rc0](https://git.duniter.org/clients/python/duniterpy/-/milestones/12) (9th July 2021)

TL;DR: The key changes are the `Documents` refactoring, the removal of the asynchronous property.
The inclusion of the examples into the package, and the introduction of `pre-commit` tool for development purposes.

### This release breaks backward compatibility!
From this version, only major releases might break backward compatibility.
For 1.x minor versions, only `DeprecationWarning` will occur.

### Features
#### Documents refactoring
Major refactoring have been done on `Document` and its sub-classes.
All documents are now internally using a `signature` attribute which is a string and no longer a list of strings, except for the `Transaction` document which can handle multi-signatures.
It is no longer possible to pass the `signature` as argument at instantiation since it doesn’t make sense in the document life cycle.
However, it is now possible to pass a `signing_key` argument to directly instantiate a signed document.
The document version and the currency arguments are now set optional and default to current document version and Ğ1 currency.
The methods to check the signature·s have been moved as specific methods into the documents classes.

- #95, !144: Remove `signatures` argument from all `__init__()`’s documents subclasses:
  - `Document` constructor is `Document(version: int, currency: str)`
  - All `Document` subclasses can be directly signed when the optional `signing_key` argument is passed
  - #172 `version` argument is now optional and have a proper default value per document type
  - `currency` argument is now optional and defaults to `G1_CURRENCY_CODENAME`
    
        signing_key: SigningKey = None,
        version: int = VERSION,
        currency: str = G1_CURRENCY_CODENAME,
    
  - Introduce `Identity.from_bma_lookup_response()` to simplify examples
  - Refactor `Document.signatures` `List` as `Document.signature` `str`
  - `VerifyingKey.verify_document()` has been removed in favour of `Document.check_signature(pubkey)`
  - Set `time` argument optional in `SimpleTransaction`

- #173, !150, !156: Make Transaction easier with single and multi-sign:
  - `Transaction(signing_keys: Optional[Union[SigningKey, List[SigningKey]]] = None)` allows single signature or multi-signatures when created
  - `Transaction.sign()` and `multi_sign()` now append signature to allow different key owner to sign additionally the Tx doc
  - Introduce `Transaction.check_signature()` and `check_signatures()`

- #178, !152: `Membership`: Make `membership_type` argument optional defaulting to `IN`

- #175, !155: Rename `timestamp` and `blockstamp` attributes and arguments depending on their content:
  - `timestamp` for time in seconds
  - `block_id` for `BlockID` type
  - `BlockUID` has been renamed to `BlockID` and `BlockID.block_uid()` to `get_block_id()` 
  
- #163, !137: `Revocation.from_signed_raw()` now stores the block ID into a `BlockID` object

#### Network
The asynchronous property has been dropped to improve DuniterPy accessibility to new comers. 
Improvements have been made on the endpoints handling.

- #140, !118: Replace `aiohttp` with non-asynchronous `http` and `ws` libraries: `urllib` and `websocket-client`:
  - `async` and `await` keywords are no longer required
  - In case you would like to keep your code asynchronous, check `request_data_async.py` example
- #170, !145: Improve `Endpoint`s’ regular expressions:
  - Add support for internationalized domain name
  - Fix `ipv4`/`host` attributes mix up
- #139, !149: Rename `Endpoint`s’ attributes from `server` to `host`
  - Rename `ConnectionHandler.server` attribute to `address`
- #174, !148: Remove `GVASUBEndpoint` (the subscription is now on `GVAEndpoint` specified with the url path)
- !138: Add `flags` and `path` attributes into `GVAEndpoint.eq()` and `hash()` computations

#### Key handling
- #150, !130: Introduce DUBP Mnemonic SigningKey feature with `SigningKey.from_dubp_mnemonic()`
  - #161, !142: Add DUBP Mnemonic example
- #181, !154: `CRCPubkey` class moved from `documents` to `key` package

#### Others
- #160, !134: Include `examples` into the Python package:
  - They can be run into Python shell
  - Read new examples usage documentation in the [README.md](README.md#Examples)
- #155, !131: Document `pip` installation
- !147: Update `websocket-client` and `attrs` to major releases

### Development Environment
`pre-commit` tool has been introduce to run our development tools as pre-commit hooks to check when committing a change.
`isort`, a tool that sorts the imports automatically, has been introduced.

- #162, !136: Introduce `mypy`, `pylint`, `isort`, `gitlab-ci` pre-commit hooks
- #132, !135: Introduce [pre-commit](https://pre-commit.com/) for `black` formatter
- #153, !129: Allow to install `black` dependency pre-releases
- !151: Add support for `mypy` v0.910
- #135, !136: Move Pylint config from the `Makefile` to `pyproject.toml`
- !132: Add support `pylint` v2.8.0

#### CI/CD
- #166, !140: Fix MR based pipelines which do not trigger the tools jobs when there is a code change
- #169, !146: Fix with a token for `gitlab-ci-lint` hook, which was no longer working since GitLab CI lint API requires authentication
- #177: Run `pre-commit` into tools jobs

### Fixes
- #134, !141: Fix license header showed into the documentation by using `#` comments rather than docstrings
  - Introduce [insert-license hook](https://github.com/Lucas-C/pre-commit-hooks)

---

- Thanks @vtexier, @Moul

## [v0.62.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/16) (16th March 2021)

### This release breaks backward compatibility !

### BEFORE
- `bma.network.peers` was pointing to `/network/peering/peers` request

### AFTER
- `bma.network.peers` point to `/network/peers` request
- `bma.network.peering_peers` point to `/network/peering/peers` request

### Features
- #141 Helper function to get best available nodes (for a real p2p client)
- #130 Allow building Block instance from local Duniter json

### Fixes
- #143 Block : fix computed_inner_hash(), sign() and proof_of_work bug
- Fixed documentation version on new release

### Development
- #147 Support pylint v1.7.2 rule R0801
- #118 Set up complete CI/CD pipeline

- Thanks @vtexier, @Moul, @matograine, @HugoTrentesaux

## [v0.61.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/14) (30th November 2020)

- #59 add GVA query support and example
- #137 add GVAEndpoint and GVASUBEndpoint classes
- fix bug in API.reverse_url

- Readme: Update dependencies list
- Upgrade to Poetry v1.1.x
- Fix regex to update the documentation version
- fix bug in pylint on examples
- Add v0.58.1 and v0.60.1 changelogs

## v0.60.1 (7th November 2020)

- #133, !116: Fix: Add support for libnacl from v1.7.2

## [v0.60.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/3) (26th September 2020)

- #60, !106: Drop Python v3.5 support
- #86, !106: Scrypt: migrate from `pylibscrypt` to `hashlib.scrypt` from the standard Python 3.6 lib
- #111, !111: Clearly define the copyright and license statements
- #68, !106: Package in Debian Bullseye v11

---

- Thanks @vtexier, @Moul

## v0.58.1 (7th November 2020)

- #133, !116: Fix: Add support for libnacl from v1.7.2

## [v0.58.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/15) (10th September 2020)

**Note**: This is the last major release with Python v3.5 support.

[As Python 3.5 security fixes have been dropped on September 13th of 2020](https://devguide.python.org/#status-of-python-branches).

### CI/CD
- #127: Change deprecated keyword in `.gitlab-ci.yml`
- #124: Fix `publish_doc` job not working

### Dependencies
- !107: Fix pylint v2.6.0 new checks
- Update black to v20.8b1, format code
- !102: Update base58 to v2
- !102: Update Sphinx to v3

### Examples
- !104: Rework send membership and identity documents examples

### Documentation
- !102: Add Repology’s packaging status to the Readme
- !103: Rename file to `CONTRIBUTING.md` to be recognised by GitLab

---

- Thanks @vtexier, @Moul

## [v0.57.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/11) (2nd April 2020)
### Code
#### Enhancements
- #123 Implement authentication from Scuttlebutt .ssb/secret file
- #77 Implement authentication from credentials file
- #122, !99 Implement `/wot/requirements-of-pending` command support in BMA api
- #120, !98 `HeadV2`, `HeadV1`, `HeadV0` and `Head` classes now each inherit from previous class **BREAK BACKWARD COMPATIBILITY**
#### Fixes
- #119, !96 `software_version` field in WS2Pv1 messages now accept a string after patch number (`1.7.21-beta` accepted)
- #114, !100 Fix `bma.blockhain.revoked` command class calling `/blockchain/with/excluded` url

### Documentation
- #104 add CONTRIBUTE.md file with release workflow
 
### CI/CD
- #66, !93  Migrate to [Poetry](https://python-poetry.org/) (build and development environment)

---

- Thanks @vtexier, @Moul

## [v0.56.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/10) (20th January 2020)
### Code
- #58, !84: Introduce WS2P API support: **BREAK BACKWARD COMPATIBILITY**:
  - `bma.ws`: now websocket connections use the special `WSConnection` class and `async`, check examples.
  - Add two examples, a helper to retrieve the WS2P API from BMA.
- #113, !89: Blocks signature verification is correctly handled now, differently from other documents.
- #112, !88: fix `Unlock.from_inline()` error on a newly created Unlock
- !87: Typos in Block: `noonce` −> `nonce`, hash
- !82: Use `sys.exit()` instead of `exit()`
- !79:
  - Move tools out of the `helper` folder
  - Dedicate the `helper` folder to helpers
  - Introduce `output_available()` helper to check if an output is available

### Dependencies
- !91: fix the `attrs`/`attr` dependency

### Build
- #107, !81: Enhance version definition in `setup.py`

### CI/CD
- !78: Release on PyPI only on tags

### Documentation
- #101, !80: Publish auto-documentation on GitLab Pages
- #106, !83: Migrate the `README` back to markdown, reword and update it.
- !77: Improve v0.55.0 and add v0.55.1 changelog
- !92: Add v0.56.0 changelog
---

- Thanks @vtexier, @Moul

## v0.55.1 (19th July 2019)
- #102: wheel build does not longer include `duniterpy` sub-folders
- Trigger release only on `master` not on tag as protected environment variables are not shared with pipelines started on a tag
- Fix Makefile: rebuild not working

## [v0.55.0](https://git.duniter.org/clients/python/duniterpy/-/milestones/9) (18th July 2019)
### Code
- Refactor request `api.ws2p.heads` in `api.bma.network.ws2p_heads` (BMA command to get ws2p heads)
- Fix bug in PubSec v1 secret key length check while loading

### Checks
#### Pylint
- Add Pylint as a dev dependency to check code validity
- Add `pylint` command to the `Makefile`
- #91, !65: Apply Pylint on the code

#### Black
- #54: Add Black as dev dependency to format the code (not PEP8 compliant)
- Add `format` and `check-format` commands to the `MakeFile`
- !63: Format the code with Black

#### Others
- `Makefile`: `check` command for `mypy`, `pylint`, and `check-format` checks
- #94: Apply Mypy, Pylint and Black on `tests` and `examples` folders

### CI
- Add MyPy, Black, and Pylint jobs
- Rename `mypy` job to `check` which run `make check`: `mypy`, `pylint`, `check-format`
- Add `check-format` job at the first stage

### Build
- Add `build` command to the Makefile
- Install build dependencies separately via a `requirements_deploy.txt` file
- #98, !72: Do not include `tests` folder in the wheel package for PyPi distribution

### CD
- Add `deploy` and `deploy_test` commands to the Makefile
- #99, !74: Add job for PyPi test deployment
- !73: Trigger PyPi release job only on tag
- Move `github-sync` stage in release stage as an `after-script` step
- #100, !75: Use extends instead of Yaml anchors in `.gitlab-ci.yml` (requires GitLab v12+)
- Also trigger the pipeline when the `Makefile` changes

### Project
- #96, !70: Add support for PEP 561 for DuniterPy type hints to be recognized by mypy when imported
- Remove not used `coveralls` dependency

## v0.54.3 (29th May 2019)
- Upload again to PyPi as previous release haven’t been uploaded thanks to the tag
- Transaction: fix `time` type

## v0.54.2 (27th May 2019)
- fix Transaction document generation
- lock transaction document generation with a test

## v0.54.1 (9th May 2019)
- `Transaction`: add __eq__() and __hash__() methods
- Transaction Unlock parameters: add __eq__() and __hash__() methods
- Transaction: add 'time' variable for read and write but not for doc generation
- output conditions: add __eq__() and __hash__() methods
- test transaction equality at all levels

---

- Thanks @Moul, @vtexier

## v0.54.0 (5th May 2019)

### Code/tests
- Fix OutputSource and InputSource from_inline() regex matching
- Transaction document: tests and code: drop versions 2 and 3 management
- Block document: code: drop vensions 2 and 3 management
- Block document: Upgrade blocks to v11 and TX to v10
- Add OutputSource.inline_condition() method
- output conditions: fix 'parser' variables default definition
- output conditions: add token() and compose() tests

### Other
- CI: Do not trigger build, tests, type check on modification of non-relevant files
- Makefile: use python3 module to run tests and type check
- Add coveralls as dev dependency
- setup.py: add classifiers: Python versions, Intended Audience
- Add CHANGELOG.md from v0.53.1

---

- Thanks @Moul, @vtexier

## v0.53.1 (18 April 2019)

- Implement equality `__eq__()` and `__hash__()` methods for InputSource and OutputSource classes

---
Thanks @Moul, @vtexier

## v0.53.0 (30 March 2019)

- To be completed…
